public class UBoot extends Boot{
    private int maxTiefe;

    public UBoot(String name, int maxTiefe){
        super(name);
        this.maxTiefe = maxTiefe;
    }

    public String toString(){
        return super.toString() + " " + maxTiefe;
    }
}
