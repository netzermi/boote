import java.util.ArrayList;
import java.util.List;

public class Hafen {
    private List<Boot> bootListe;

    public Hafen(){
        this.bootListe = new ArrayList<>();
    }

    public void bootHinzufügen(Boot boot){
        bootListe.add(boot);
    }

    public void alleBooteAusgeben(){
        for(Boot b : bootListe){
            System.out.println(b.toString());
        }
    }


}
