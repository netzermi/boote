public class Boot {
    private String name;

    public Boot(String name){
        this.name = name;
    }

    public String toString(){
        return name;
    }
}
