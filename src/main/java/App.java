public class App {

    public static void main(String[] args) {
        Schlauchboot b1 = new Schlauchboot("Franz");
        UBoot b2 = new UBoot("Mary", 1200);
        Hafen hafen = new Hafen();
        hafen.bootHinzufügen(b1);
        hafen.bootHinzufügen(b2);

        hafen.alleBooteAusgeben();
    }
}
